import React, { useState, useEffect } from 'react';
import { Text, StyleSheet, View, FlatList, TouchableHighlight, TouchableWithoutFeedback, Keyboard, Platform } from 'react-native';
import Appointment from './components/Appointment';
import Form from './components/Form';
import AsyncStorage from '@react-native-community/async-storage'

const App = () => {

  const [showForm, setShowForm] = useState(false);
  // Definimos el state de citas
  const [citas, setCitas] = useState([]);

  useEffect(() => {
    const getAppointmentsStorage = async () => {
      try {
        const appointmentsStorage = await AsyncStorage.getItem('appointments');
        if(appointmentsStorage){
          setCitas(JSON.parse(appointmentsStorage))
        }
      } catch (error) {
        console.error(error)
      }
    }
    getAppointmentsStorage();
  }, [])

  // Delete the patient of the state
  const deletePatient = id => {
    const filteredAppointments =  citas.filter( cita => cita.id !== id)
    setCitas(filteredAppointments);
    saveAppointmentStorage(JSON.stringify(filteredAppointments));
    // setCitas((actualAppointments) => {
    //   return actualAppointments.filter(appointment => appointment.id !== id)
    // })
  }

  // Show or hide the form
  const showFormFn = () => {
    setShowForm(!showForm);
  }

  // Hide the keyboard
  const closeKeyboard = () => {
    Keyboard.dismiss();
  }

  // Save the appointments in the Storage
  const saveAppointmentStorage = async (appointmentsJSON) => {
    try {
      await AsyncStorage.setItem('appointments', appointmentsJSON);
    } catch (error) {
      console.error(error)
    }
  }

  return (
    <>
      {/* <TouchableWithoutFeedback onPress={() => closeKeyboard()}> */}
        <View style={styles.contenedor}>
          <Text style={styles.titulo}>Administrador de Citas</Text>

          <View style={styles.content}>

            {showForm ? (
              <>
                <View>
                  <TouchableHighlight onPress={() => showFormFn()} style={styles.btnCancel}>
                    <Text style={styles.txtAdd}>Cancel</Text>
                  </TouchableHighlight>
                </View>
                <Form
                  citas={citas}
                  setCitas={setCitas}
                  setShowForm={setShowForm}
                  saveAppointmentStorage = {saveAppointmentStorage}
                />
              </>
            ) :
              (
                <>
                  <View>
                    <TouchableHighlight onPress={() => showFormFn()} style={styles.btnAdd}>
                      <Text style={styles.txtAdd}>Add new Appointment</Text>
                    </TouchableHighlight>
                  </View>
                  <Text style={styles.titulo}>{citas.length > 0 ? 'Manage your appointments' : 'There are not appointments, create a new one'}</Text>

                  {/* Forma RN, mejora en PERFORMANCE */}
                  <FlatList
                    style={styles.list}
                    data={citas}
                    renderItem={({ item }) => <Appointment item={item} deletePatient={deletePatient} />}
                    keyExtractor={cita => cita.id}
                  />
                  {/* Forma React */}
                  {/* {citas.map(cita => (
                    <View>
                    <Text>{cita.paciente}</Text>
                    </View>
                ))} */}
                </>
              )}
          </View>
        </View>
      {/* </TouchableWithoutFeedback> */}

    </>
  );
};

const styles = StyleSheet.create({
  contenedor: {
    backgroundColor: '#AA076B',
    flex: 1
  },
  titulo: {
    color: '#FFF',
    textAlign: 'center',
    marginTop: Platform.os === 'ios' ? 40 : 20,
    marginBottom: 20,
    paddingHorizontal: 20,
    fontSize: 24,
    fontWeight: 'bold'
  },
  content: {
    flex: 1,
    marginHorizontal: '2.5%',
  },
  list: {
    flex: 1,
  },
  btnAdd: {
    padding: 10,
    backgroundColor: '#8FB22E',
    marginVertical: 10
  },
  txtAdd: {
    color: '#FFF',
    fontWeight: 'bold',
    textAlign: 'center',
    textTransform: 'uppercase'
  },
  btnCancel: {
    padding: 10,
    backgroundColor: '#D11019',
    marginVertical: 30
  }
});


export default App;
