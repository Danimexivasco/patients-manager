import React, { useState } from 'react';
import { Text, StyleSheet, View, TextInput, KeyboardAvoidingView, Button, Platform, TouchableHighlight, ALert, Alert, ScrollView } from 'react-native';
import DateTimePickerModal from "react-native-modal-datetime-picker";
import shortid from 'shortid';


const Form = ({ citas, setCitas, setShowForm, saveAppointmentStorage }) => {

    const [patient, setPatient] = useState('');
    const [owner, setOwner] = useState('');
    const [telephoneNumber, setTelephoneNumber] = useState('');
    const [dateInput, setDateInput] = useState('');
    const [timeInput, setTimeInput] = useState('');
    const [symptoms, setSymptoms] = useState('');

    const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
    const [isTimePickerVisible, setTimePickerVisibility] = useState(false);

    const showDatePicker = () => {
        setDatePickerVisibility(true);
    };

    const hideDatePicker = () => {
        setDatePickerVisibility(false);
    };

    const handleConfirmDate = (date) => {
        if (Platform.OS === 'ios') {
            const options = { year: 'numeric', month: 'long', day: "2-digit" }
            setDateInput(date.toLocaleDateString('es-ES', options))
        } else {
            const inputDate = new Date(date);
            const day = inputDate.getDate() < 10 ? `0${inputDate.getDate()}` : inputDate.getDate();
            const month = inputDate.getMonth() < 10 ? `0${inputDate.getMonth() + 1}` : inputDate.getMonth() + 1;
            const year = inputDate.getFullYear();
            setDateInput(`${day}/${month}/${year}`);
        }
        hideDatePicker();
    };

    // Shows or hide the Time Picker
    const showTimePicker = () => {
        setTimePickerVisibility(true);
    };

    const hideTimePicker = () => {
        setTimePickerVisibility(false);
    };

    const handleConfirmTime = (time) => {
        if (Platform.OS === 'ios') {
            const options = { hour12: false, hour: '2-digit', minute: '2-digit' };
            setTimeInput(time.toLocaleTimeString('es-ES', options))
        } else {
            const inputDate = new Date(time);
            const hours = inputDate.getHours() < 10 ? `0${inputDate.getHours()}` : inputDate.getHours();
            const minutes = inputDate.getMinutes() < 10 ? `0${inputDate.getMinutes()}` : inputDate.getMinutes();
            setTimeInput(`${hours}:${minutes}`)
        }
        hideTimePicker();
    };

    const createNewAppointment = () => {
        if (patient.trim() === '' || owner.trim() === '' || telephoneNumber.trim() === '' || dateInput.trim() === '' || timeInput.trim() === '' || symptoms.trim() === '') {
            // Validation Fails
            showAlert();
            return;
        }

        // Create new Appointment
        const appointment = { patient, owner, telephoneNumber, dateInput, timeInput, symptoms };
        appointment.id = shortid.generate();
        console.log(appointment);

        // Add to the state
        const newAppointments = [...citas, appointment];
        setCitas(newAppointments);

        // Save the appointments in the storage
        saveAppointmentStorage(JSON.stringify(newAppointments))

        // Hide the form
        setShowForm(false);

        // Reset the form
        // setPatient('');
        // setOwner('');
        // setTelephoneNumber('');
        // setDateInput('');
        // setTimeInput('');
        // setSymptoms('');
    }

    // Show alert if validation fails
    const showAlert = () => {
        Alert.alert(
            'Error',    //The title
            'All fields are required',  //Message
            [{
                text: 'OK'  //Array of buttons
            }]
        )
    }

    return (
        <>
            <ScrollView style={styles.formContainer} >
                <Text style={styles.title}>Add New Appointment</Text>
                <View>
                    <Text style={styles.label}>Patient:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => setPatient(text)}
                    />
                </View>
                <View>
                    <Text style={styles.label}>Owner:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => setOwner(text)}
                    />
                </View>
                <View>
                    <Text style={styles.label}>Contact number:</Text>
                    <TextInput
                        style={styles.input}
                        onChangeText={(text) => setTelephoneNumber(text)}
                        keyboardType='phone-pad'
                    />
                </View>
                <View>
                    <Text style={styles.label}>Date:</Text>
                    <Button title="Select Date" onPress={showDatePicker} />
                    <DateTimePickerModal
                        isVisible={isDatePickerVisible}
                        mode="date"
                        onConfirm={handleConfirmDate}
                        onCancel={hideDatePicker}
                        locale='es_ES'
                        headerTextIOS="Elige una Fecha"
                        cancelTextIOS="Cancelar"
                        confirmTextIOS="Confirmar"
                    />
                    <Text>{dateInput}</Text>
                </View>
                <View>
                    <Text style={styles.label}>Time:</Text>
                    <Button title="Select Time" onPress={showTimePicker} />
                    <DateTimePickerModal
                        isVisible={isTimePickerVisible}
                        mode="time"
                        onConfirm={handleConfirmTime}
                        onCancel={hideTimePicker}
                        locale='es_ES'
                        headerTextIOS="Elige una Hora"
                        cancelTextIOS="Cancelar"
                        confirmTextIOS="Confirmar"
                        is24Hour
                    />
                    <Text>{timeInput}</Text>
                </View>
                <View>
                    <Text style={styles.label}>Symptoms:</Text>
                    <TextInput
                        multiline
                        style={styles.input}
                        onChangeText={(text) => setSymptoms(text)}
                    />
                </View>
                <View>
                    <TouchableHighlight onPress={() => createNewAppointment()} style={styles.btnSubmit}>
                        <Text style={styles.txtSubmit}>Add </Text>
                    </TouchableHighlight>
                </View>
            </ScrollView>
        </>
    );
}

const styles = StyleSheet.create({
    formContainer: {
        backgroundColor: '#FFF',
        paddingHorizontal: 20,
        paddingVertical: 10,
    },
    title: {
        textAlign: 'center',
        marginTop: 20,
        marginBottom: 20,
        paddingHorizontal: 20,
        fontSize: 24,
        fontWeight: 'bold'
    },
    label: {
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 20
    },
    input: {
        marginTop: 10,
        height: 50,
        borderColor: '#e1e1e1',
        borderWidth: 1,
        borderStyle: 'solid'
    },
    btnSubmit: {
        padding: 10,
        backgroundColor: '#8FB22E',
        marginVertical: 30,
    },
    txtSubmit: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
        textTransform: 'uppercase'
    }
})

export default Form;