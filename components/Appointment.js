import React, { useState } from 'react';
import { Text, StyleSheet, View, Button, TouchableHighlight } from 'react-native';

const Appointment = ({ item, deletePatient }) => {


    const deleteDialog = (id) => {
        console.log('Deleting...', id)
        deletePatient(id);
        
    }

    return (
        <View style={styles.cita}>
            <View>
                <Text style={styles.label}>Paciente:</Text>
                <Text style={styles.texto}>{item.patient}</Text>
            </View>
            <View>
                <Text style={styles.label}>Propietario:</Text>
                <Text style={styles.texto}>{item.owner}</Text>
            </View>
            <View>
                <Text style={styles.label}>Sintomas:</Text>
                <Text style={styles.texto}>{item.symptoms}</Text>
            </View>
            <View>
                <TouchableHighlight onPress={() => deleteDialog(item.id)} style={styles.btnDelete}>
                    <Text style={styles.txtDelete}>Eliminar &times;</Text>
                </TouchableHighlight>
            </View>
            {/* <Button 
                title="Eliminar"
            /> */}
        </View>
    );
}

const styles = StyleSheet.create({
    cita: {
        backgroundColor: '#FFF',
        borderBottomColor: '#e1e1e1',
        borderStyle: 'solid',
        borderBottomWidth: 1,
        paddingVertical: 20,
        paddingHorizontal: 20
        // paddingTop: 20,
        // paddingBottom: 20
    },
    label:{
        fontWeight: 'bold',
        fontSize: 18,
        marginTop: 20
    },
    texto: {
        fontSize: 18,
    },
    btnDelete: {
        padding: 10,
        backgroundColor: '#D11019',
        marginVertical: 10
    },
    txtDelete: {
        color: '#FFF',
        fontWeight: 'bold',
        textAlign: 'center',
        textTransform: 'uppercase'
    }
})

export default Appointment;